package com.cowboy.shooter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@Data
@EqualsAndHashCode
@Entity
public class TeamMatchState extends DataEntity {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "bullets")
    private Integer bullets;

    @Column(name = "blockCounter")
    private Integer blockCounter;

    @Column(name = "evadeCounter")
    private Integer evadeCounter;

    @Column(name = "isDead")
    private Boolean isDead;

}
