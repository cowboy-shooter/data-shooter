package com.cowboy.shooter.model;

import com.cowboy.shooter.dto.type.ProgrammingLanguagesType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Team extends DataEntity {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "name", nullable = false, unique = true, length = 25)
    private String name;

    @Column(name = "url", nullable = false)
    private String url;

    @Column(name = "language", nullable = false)
    private ProgrammingLanguagesType language;

    @Column(name = "description", length = 150)
    private String description;

    @Lob
    @Column(name = "flag")
    @Basic(fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private String flag;

    @OneToMany(mappedBy = "team")
    @EqualsAndHashCode.Exclude
    private List<User> members = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private User leader;

    @ManyToMany
    @EqualsAndHashCode.Exclude
    private List<User> invitees = new ArrayList<>();

    @ManyToMany
    @EqualsAndHashCode.Exclude
    private List<User> requestors = new ArrayList<>();

    @ManyToMany
    @EqualsAndHashCode.Exclude
    private List<Team> challengedTeams = new ArrayList<>();



}
