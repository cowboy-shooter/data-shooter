package com.cowboy.shooter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class TournamentTier extends DataEntity {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "executed")
    private Boolean executed;

    @ManyToMany
    @EqualsAndHashCode.Exclude
    private List<Team> teams = new ArrayList<>();

    @OneToMany
    @EqualsAndHashCode.Exclude
    private List<TournamentMatch> tournamentMatches = new ArrayList<>();

}
