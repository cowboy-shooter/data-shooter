package com.cowboy.shooter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Data
@Entity
public class PlannedMatch extends DataEntity {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @OneToOne
    @EqualsAndHashCode.Exclude
    private Team team1;

    @OneToOne
    @EqualsAndHashCode.Exclude
    private Team team2;

    @OneToOne
    @EqualsAndHashCode.Exclude
    private Match executedMatch;

}
