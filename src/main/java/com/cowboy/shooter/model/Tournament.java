package com.cowboy.shooter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Entity
public class Tournament extends DataEntity {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "name")
    private String name;

    @OneToOne(fetch = FetchType.LAZY)
    private Team winner;

    @OneToOne(fetch = FetchType.LAZY)
    private Team secondPlace;

    @OneToOne(fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private TournamentTier firstTier;

    @OneToOne(fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private TournamentTier secondTier;

    @OneToOne(fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private TournamentTier thirdTier;

    @OneToOne(fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private TournamentTier fourthTier;

    @Column(name = "started")
    private LocalDateTime started;

}
