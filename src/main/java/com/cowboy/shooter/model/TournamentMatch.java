package com.cowboy.shooter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class TournamentMatch extends DataEntity {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "executed")
    private Boolean executed;

    @Column(name = "team1Score")
    private Integer team1Score;

    @Column(name = "team2Score")
    private Integer team2Score;

    @OneToOne
    @EqualsAndHashCode.Exclude
    private Team team1;

    @OneToOne
    @EqualsAndHashCode.Exclude
    private Team team2;

    @OneToMany
    @EqualsAndHashCode.Exclude
    private List<PlannedMatch> plannedMatches = new ArrayList<>();

}
