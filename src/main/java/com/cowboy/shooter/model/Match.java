package com.cowboy.shooter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Data
@EqualsAndHashCode
@Entity
public class Match extends DataEntity {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "executedInTournament")
    private Boolean executedInTournament;

    @OneToOne(fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private Team team1;

    @OneToOne(fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private Team team2;

    @OneToMany
    @EqualsAndHashCode.Exclude
    private List<MatchMove> moves = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private Team winner;

    @Column(name = "started")
    private LocalDateTime started;

    @Column(name = "finished")
    private LocalDateTime finished;


}
