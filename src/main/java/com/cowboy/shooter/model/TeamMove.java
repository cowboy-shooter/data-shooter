package com.cowboy.shooter.model;

import com.cowboy.shooter.dto.TeamMoveType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@Data
@EqualsAndHashCode
@Entity
public class TeamMove extends DataEntity {
    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "teamMove")
    private TeamMoveType teamMove;

    @Column(name = "faulty")
    private boolean faulty;

    @Column(name = "message", length = 2048)
    private String message;

}
