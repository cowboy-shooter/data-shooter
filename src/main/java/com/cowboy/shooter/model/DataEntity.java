package com.cowboy.shooter.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Data
@EqualsAndHashCode
public class DataEntity implements Serializable {
}
