package com.cowboy.shooter.model;

import com.cowboy.shooter.dto.TeamMoveType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Getter
@Setter
@Data
@EqualsAndHashCode
@Entity
public class MatchMove extends DataEntity {

    @Id
    @Column(name = "ID", nullable = false)
    @GeneratedValue(generator = "uuid", strategy = GenerationType.AUTO)
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    @Column(name = "sequenceNumber")
    private Integer sequenceNumber;

    @OneToOne(cascade = CascadeType.ALL)
    private TeamMove team1Move;

    @OneToOne(cascade = CascadeType.ALL)
    private TeamMove team2Move;

    @OneToOne(fetch = FetchType.LAZY)
    private TeamMatchState team1State;

    @OneToOne(fetch = FetchType.LAZY)
    private TeamMatchState team2State;
}
