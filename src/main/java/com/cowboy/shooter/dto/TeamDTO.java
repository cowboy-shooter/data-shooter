package com.cowboy.shooter.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
public class TeamDTO implements Serializable {

    private String id;
    private String name;
    private String url;

}
