package com.cowboy.shooter.dto.wrapper;

import com.cowboy.shooter.dto.type.TeamUserManagementType;
import com.cowboy.shooter.model.User;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class TeamUserManagementWrapper {
    private User user;
    private TeamUserManagementType teamUserManagementType;

    public TeamUserManagementWrapper(User user, TeamUserManagementType teamUserManagementType) {
        this.user = user;
        this.teamUserManagementType = teamUserManagementType;
    }
}
