package com.cowboy.shooter.dto.wrapper;

import com.cowboy.shooter.dto.type.TeamChallengesType;
import com.cowboy.shooter.model.Team;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class TeamChallengesManagementWrapper {
    private Team team;
    private TeamChallengesType teamChallengesType;

    public TeamChallengesManagementWrapper(Team team, TeamChallengesType teamChallengesType) {
        this.team = team;
        this.teamChallengesType = teamChallengesType;
    }
}
