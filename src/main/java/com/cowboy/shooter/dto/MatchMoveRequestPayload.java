package com.cowboy.shooter.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Data
public class MatchMoveRequestPayload implements Serializable {

    private Integer sequenceNumber;
    private TeamMoveType opponentsLastMove;
    private String opponentId;
    private String opponentName;

    public MatchMoveRequestPayload(Integer sequenceNumber, TeamMoveType opponentsLastMove, String opponentId, String opponentName) {
        this.sequenceNumber = sequenceNumber;
        this.opponentsLastMove = opponentsLastMove;
        this.opponentId = opponentId;
        this.opponentName = opponentName;
    }
}
