package com.cowboy.shooter.dto;

public enum TeamMoveType {
    BLOCK,
    SHOOT,
    EVADE,
    RELOAD,
    NULL
}
