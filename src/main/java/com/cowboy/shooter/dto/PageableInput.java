package com.cowboy.shooter.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageableInput {

    private Integer page;
    private Integer size;
    private Sort.Direction sortDirection;
    private String sortBy;

    public static PageRequest convert(PageableInput inputPage) {
        String sortBy = inputPage.sortBy != null ? inputPage.sortBy : "id";
        int page = inputPage.page != null ? inputPage.page : 0;
        int size = inputPage.size != null ? inputPage.size : 20;
        return PageRequest.of(page, size, Sort.by(inputPage.sortDirection != null ? inputPage.sortDirection : Sort.Direction.DESC, sortBy));
    }
}
