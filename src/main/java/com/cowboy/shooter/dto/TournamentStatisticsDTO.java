package com.cowboy.shooter.dto;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
@Data
public class TournamentStatisticsDTO {

    private Map<String, Integer> failResponses = new HashMap<>();
    private Map<String, Integer> bulletsShot = new HashMap<>();
    private Map<String, Integer> evades = new HashMap<>();
    private Map<String, Integer> blocks = new HashMap<>();
    private Map<String, Integer> reloads = new HashMap<>();
    private Map<String, Integer> unsuccessfulBlocks = new HashMap<>();
    private Map<String, Integer> unsuccessfulReloads = new HashMap<>();
    private Map<String, Integer> unsuccessfulEvades = new HashMap<>();
    private Map<String, Integer> unsuccessfulShots = new HashMap<>();
    private Map<String, Integer> wins = new HashMap<>();
    private Map<String, Integer> loses = new HashMap<>();
    private Map<String, Integer> draws = new HashMap<>();
    private Map<String, Integer> blockedShots = new HashMap<>();
    private Map<String, Integer> evadedShots = new HashMap<>();
    private Map<String, Integer> score = new HashMap<>();

}
