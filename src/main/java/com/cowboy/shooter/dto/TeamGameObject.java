package com.cowboy.shooter.dto;

import com.cowboy.shooter.config.types.GameConfigLimits;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
public class TeamGameObject implements Serializable {

    private int bullets;
    private int blockCounter;
    private int evadeCounter;
    private Boolean isDead = false;

    private TeamDTO team;
    private GameConfigLimits limits;

    public TeamGameObject(TeamDTO team, GameConfigLimits limits) {
        this.team = team;
        this.limits = limits;
    }

    public void reload() {
        blockCounter = 0;
        if (canReload()) {
            bullets++;
        }
    }

    public void shoot() {
        blockCounter = 0;
        if (canShoot()) {
            bullets--;
        }
    }

    public void evade() {
        blockCounter = 0;
        if (canEvade()) {
            evadeCounter++;
        }
    }

    public void block() {
        if (canBlock()) {
            blockCounter++;
        }
    }

    public boolean canBlock() {
        return blockCounter < limits.getMaximumBlocksInRow();
    }

    public boolean canEvade() {
        return evadeCounter < limits.getMaximumEvades();
    }

    public boolean canReload() {
        return bullets < limits.getMaximumBullets();
    }

    public boolean canShoot() {
        return bullets > 0;
    }

}
