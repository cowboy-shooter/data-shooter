package com.cowboy.shooter.dto.type;

public enum ProgrammingLanguagesType {
    JAVA ("Java"),
    DOT_NET(".Net"),
    C_SHARP("C#"),
    JAVASCRIPT("Javascript"),
    ARNOLD_C("Arnold C");

    private final String name;

    ProgrammingLanguagesType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
