package com.cowboy.shooter.dto.type;

public enum TeamUserManagementType {
    INVITED, REQUESTOR, NONE
}
