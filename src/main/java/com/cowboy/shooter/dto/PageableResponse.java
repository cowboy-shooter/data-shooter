package com.cowboy.shooter.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class PageableResponse<T> {

    private PageInfo pageInfo;

    private List<T> content = new ArrayList<>();

    public PageableResponse(Page<T> page) {
        this.pageInfo = new PageInfo(page);
        this.content = page.getContent();
    }

}