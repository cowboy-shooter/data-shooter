package com.cowboy.shooter.dto;

import com.cowboy.shooter.model.TeamMatchState;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

@Getter
@Setter
@Data
@EqualsAndHashCode
public class MatchMoveDTO implements Serializable {

    private Integer sequenceNumber;
    private TeamMoveType team1Move;
    private TeamMoveType team2Move;
    private TeamMatchState team1State;
    private TeamMatchState team2State;

}
