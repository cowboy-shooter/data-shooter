package com.cowboy.shooter.dto;

import com.cowboy.shooter.model.TeamMove;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Data
@EqualsAndHashCode
public class TeamMoveWrapper {
    TeamMove team1Move;
    TeamMove team2Move;

    public TeamMoveWrapper(TeamMove team1Move, TeamMove team2Move) {
        this.team1Move = team1Move;
        this.team2Move = team2Move;
    }
}
