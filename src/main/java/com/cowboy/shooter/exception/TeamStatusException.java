package com.cowboy.shooter.exception;

public class TeamStatusException extends RuntimeException {

    public TeamStatusException() {
    }

    public TeamStatusException(String message) {
        super(message);
    }

    public TeamStatusException(String message, Throwable cause) {
        super(message, cause);
    }

    public TeamStatusException(Throwable cause) {
        super(cause);
    }

    public TeamStatusException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
