package com.cowboy.shooter.service;

import com.cowboy.shooter.dto.PageableResponse;
import com.cowboy.shooter.model.Match;
import com.cowboy.shooter.model.MatchMove;
import com.cowboy.shooter.model.Team;
import com.cowboy.shooter.repository.MatchMoveRepository;
import com.cowboy.shooter.repository.MatchRepository;
import com.cowboy.shooter.repository.TeamMatchStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class MatchService {

    @Autowired
    private TeamService teamService;

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private MatchMoveRepository matchMoveRepository;

    @Autowired
    private TeamMatchStateRepository teamMatchStateRepository;


    @Transactional
    public void addMove(String matchId, MatchMove move) {
        Match match = getMatch(matchId).get();
        move.setTeam1State(teamMatchStateRepository.save(move.getTeam1State()));
        move.setTeam2State(teamMatchStateRepository.save(move.getTeam2State()));
        move = matchMoveRepository.save(move);
        match.getMoves().add(move);
        matchRepository.save(match);
    }

    @Transactional
    public void finishMatch(String matchId, String winnerTeamID) {
        Match match = getMatch(matchId).get();
        if (match.getTeam1().getId().equals(winnerTeamID)) {
            match.setWinner(match.getTeam1());
        } else if (match.getTeam2().getId().equals(winnerTeamID)) {
            match.setWinner(match.getTeam2());
        }
        match.setFinished(LocalDateTime.now());
        matchRepository.save(match);
    }

    @Transactional(readOnly = true)
    public Optional<Match> getMatch(String matchId) {
        return matchRepository.findById(matchId);
    }

    @Transactional
    public Match createMatch(String team1Id, String team2Id, boolean executedInTournament) {
        Team team1 = teamService.getTeam(team1Id).get();
        Team team2 = teamService.getTeam(team2Id).get();
        Match match = new Match();

        match.setTeam1(team1);
        match.setTeam2(team2);
        match.setExecutedInTournament(executedInTournament);
        match.setStarted(LocalDateTime.now());
        return matchRepository.save(match);
    }

    @Transactional(readOnly = true)
    public PageableResponse<Match> getMatches(Pageable pageable) {
        return new PageableResponse(matchRepository.findAll(pageable));
    }

    public List<Match> getParticipatedMatches(String teamId) {
        Team team = teamService.getTeam(teamId).get();
        return matchRepository.findByTeam1OrTeam2(team, team);
    }
}
