package com.cowboy.shooter.service;

import com.cowboy.shooter.game.MatchExecutor;
import com.cowboy.shooter.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MatchExecutingService {

    @Autowired
    private MatchService matchService;

    @Autowired
    private MatchExecutor matchExecutor;

    @Autowired
    private TeamService teamService;

    @Autowired
    private UserTeamService userTeamService;


    public Match startMatchAsync(String team1Id, String team2Id, boolean executedInTournament) {
        Match match = matchService.createMatch(team1Id, team2Id, executedInTournament);
        matchExecutor.executeMatchAsync(match.getId(), teamService.getTeamDto(team1Id), teamService.getTeamDto(team2Id));
        return match;
    }

    public Match startMatchSync(String team1Id, String team2Id, boolean executedInTournament) {
        Match match = matchService.createMatch(team1Id, team2Id, executedInTournament);
        matchExecutor.executeMatch(match.getId(), teamService.getTeamDto(team1Id), teamService.getTeamDto(team2Id));
        return match;
    }

    public Match acceptChallenge(String teamId) {
        String ownTeamId = userTeamService.getOwnTeam().getId();
        if (userTeamService.acceptChallenge(teamId)) {
            Match match = startMatchAsync(ownTeamId, teamId, false);
            return match;
        }
        return null;
    }

    public void executeTier(TournamentTier tier) {
        for (TournamentMatch tournamentMatch: tier.getTournamentMatches()) {
            for (PlannedMatch plannedMatch: tournamentMatch.getPlannedMatches()) {
                Match match = startMatchSync(plannedMatch.getTeam1().getId(), plannedMatch.getTeam2().getId(), true);
                plannedMatch.setExecutedMatch(match);
            }
        }
    }

}
