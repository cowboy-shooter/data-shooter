package com.cowboy.shooter.service.tournament;

import com.cowboy.shooter.model.PlannedMatch;
import com.cowboy.shooter.model.Team;
import com.cowboy.shooter.repository.PlannedMatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlannedMatchService {

    @Autowired
    private PlannedMatchRepository plannedMatchRepository;

    @Transactional
    public PlannedMatch savePlannedMatch(PlannedMatch plannedMatch) {
        return plannedMatchRepository.save(plannedMatch);
    }


    public List<PlannedMatch> createPlannedMatches(Team team1, Team team2, int matchesCount) {
        List<PlannedMatch> plannedMatches = new ArrayList<>();

        for (int i = 0; i < matchesCount; i++) {
            PlannedMatch plannedMatch = new PlannedMatch();

            plannedMatch.setTeam1(team1);
            plannedMatch.setTeam2(team2);

            savePlannedMatch(plannedMatch);

            plannedMatches.add(plannedMatch);
        }

        return plannedMatches;
    }
}
