package com.cowboy.shooter.service.tournament;

import com.cowboy.shooter.base.BooleanUtils;
import com.cowboy.shooter.dto.PageableResponse;
import com.cowboy.shooter.dto.TournamentStatisticsDTO;
import com.cowboy.shooter.exception.TournamentException;
import com.cowboy.shooter.model.*;
import com.cowboy.shooter.repository.TournamentRepository;
import com.cowboy.shooter.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class TournamentService {

    @Autowired
    private TeamService teamService;

    @Autowired
    private TournamentTierService tournamentTierService;

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private TournamentMatchService tournamentMatchService;

    @Autowired
    private TournamentStatisticsService tournamentStatisticsService;

    public Tournament createTournament(List<String> teamIds, String name) {
        List<Team> teams = teamService.getTeams(teamIds);
        Tournament tournament = new Tournament();
        tournament.setStarted(LocalDateTime.now());
        tournament.setName(name);
        tournament.setFirstTier(tournamentTierService.createFirstTier(teams));

        return saveTournament(tournament);
    }

    public void executeTier(String tierId) {
        Tournament tournament = tournamentRepository.findByFirstTier_IdOrSecondTier_IdOrThirdTier_IdOrFourthTier_Id(tierId, tierId, tierId, tierId);
        TournamentTier foundTier = null;
        if (tournament.getFirstTier().getId().equals(tierId)) {
            foundTier = tournament.getFirstTier();
        } else if (tournament.getSecondTier().getId().equals(tierId)) {
            foundTier = tournament.getSecondTier();
        } else if (tournament.getThirdTier().getId().equals(tierId)) {
            foundTier = tournament.getThirdTier();
        } else if (tournament.getFourthTier().getId().equals(tierId)) {
            foundTier = tournament.getFourthTier();
        }

        if (foundTier != null && BooleanUtils.getBooleanValue(foundTier.getExecuted()) ) {
            throw new TournamentException("Tier [" + tierId + "] was already executed");
        } else if (foundTier == null) {
            throw new TournamentException("Tier [" + tierId + "] was not found");
        }

        tournamentTierService.executeTier(tierId);
    }

    public PageableResponse<Tournament> getAllTournaments(Pageable pageable) {
        return new PageableResponse(tournamentRepository.findAll(pageable));
    }

    public List<Tournament> getAllTournaments() {
        return tournamentRepository.findAll();
    }

    @Transactional
    public Tournament saveTournament(Tournament tournament) {
        return tournamentRepository.save(tournament);
    }

    @Transactional
    public Optional<Tournament> getTournament(String id) {
        Optional<Tournament> optionalTournament = tournamentRepository.findById(id);
        if (optionalTournament.isPresent()) {
            Tournament tournament = optionalTournament.get();
            updateTiers(tournament);
        }

        return optionalTournament;
    }

    public void updateTiers(Tournament tournament) {
        if (tournament.getFourthTier() != null && BooleanUtils.getBooleanValue(tournament.getFourthTier().getExecuted())) {
            return;
        }

        if (tournament.getFourthTier() != null
                && !BooleanUtils.getBooleanValue(tournament.getFourthTier().getExecuted())
                && arePlannedMatchesExecuted(tournament.getFourthTier())) {

            TournamentTier tier = tournament.getFourthTier();
            tier.setExecuted(true);
            TournamentMatch match = tier.getTournamentMatches().get(0);
            if (match.getTeam1Score() > match.getTeam2Score()) {
                tournament.setWinner(match.getTeam1());
                tournament.setSecondPlace(match.getTeam2());
            } else if (match.getTeam2Score() > match.getTeam1Score()) {
                tournament.setWinner(match.getTeam2());
                tournament.setSecondPlace(match.getTeam1());
            }
            tournamentTierService.saveTier(tier);
            saveTournament(tournament);

        } else if (tournament.getThirdTier() != null
                && !BooleanUtils.getBooleanValue(tournament.getThirdTier().getExecuted())
                && arePlannedMatchesExecuted(tournament.getThirdTier())) {

            TournamentTier tier = tournament.getThirdTier();
            tier.setExecuted(true);
            tournamentTierService.saveTier(tier);
            tournament.setFourthTier(tournamentTierService.createFourthTier(tier));
            saveTournament(tournament);

        } else if (tournament.getSecondTier() != null
                && !BooleanUtils.getBooleanValue(tournament.getSecondTier().getExecuted())
                && arePlannedMatchesExecuted(tournament.getSecondTier())) {

            TournamentTier tier = tournament.getSecondTier();
            tier.setExecuted(true);
            tournamentTierService.saveTier(tier);
            tournament.setThirdTier(tournamentTierService.createThirdTier(tournament.getFirstTier(), tier));
            saveTournament(tournament);

        } else if (tournament.getFirstTier() != null
                && !BooleanUtils.getBooleanValue(tournament.getFirstTier().getExecuted())
                && arePlannedMatchesExecuted(tournament.getFirstTier())) {

            TournamentTier tier = tournament.getFirstTier();
            tier.setExecuted(true);
            tournamentTierService.saveTier(tier);
            tournament.setSecondTier(tournamentTierService.createSecondTier(tier));
            saveTournament(tournament);
        }
    }

    public boolean arePlannedMatchesExecuted(TournamentTier tier) {
        for (TournamentMatch match: tier.getTournamentMatches()) {
            TournamentMatch resolvedMatch = tournamentMatchService.resolveMatch(match);
            if (!BooleanUtils.getBooleanValue(resolvedMatch.getExecuted())) {
                return false;
            }
        }
        return true;
    }

    public Tournament fixTier(String id, List<String> teamIds) {
        Tournament tournament = tournamentRepository.findByFirstTier_IdOrSecondTier_IdOrThirdTier_IdOrFourthTier_Id(id, id, id, id);
        List<Team> teams = teamService.getTeams(teamIds);

        if (tournament.getFirstTier().getId().equals(id)) {
            tournament.setFirstTier(tournamentTierService.createFirstTier(teams));
        } else if (tournament.getSecondTier().getId().equals(id)) {
            tournament.setSecondTier(tournamentTierService.createSecondTier(teams));
        } else if (tournament.getThirdTier().getId().equals(id)) {
            tournament.setThirdTier(tournamentTierService.createThirdTier(teams));
        } else if (tournament.getFourthTier().getId().equals(id)) {
            tournament.setFourthTier(tournamentTierService.createFourthTier(teams));
        }

        return saveTournament(tournament);
    }

    public TournamentStatisticsDTO getStatistics(String id) {
        return tournamentStatisticsService.getStatistics(getTournament(id).get());
    }
}
