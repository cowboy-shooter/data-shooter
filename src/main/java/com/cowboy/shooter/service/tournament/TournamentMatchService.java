package com.cowboy.shooter.service.tournament;

import com.cowboy.shooter.base.BooleanUtils;
import com.cowboy.shooter.config.GameConfig;
import com.cowboy.shooter.config.types.TournamentConfig;
import com.cowboy.shooter.model.Match;
import com.cowboy.shooter.model.PlannedMatch;
import com.cowboy.shooter.model.TournamentMatch;
import com.cowboy.shooter.repository.TournamentMatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class TournamentMatchService {

    @Autowired
    private TournamentMatchRepository tournamentMatchRepository;

    @Autowired
    private GameConfig gameConfig;

    @Transactional
    public TournamentMatch saveTournamentMatch(TournamentMatch tournamentMatch) {
        return tournamentMatchRepository.save(tournamentMatch);
    }

    public TournamentMatch resolveMatch(TournamentMatch tournamentMatch) {
        if (!BooleanUtils.getBooleanValue(tournamentMatch.getExecuted())) {
            int team1Wins = 0;
            int team2Wins = 0;
            int draws = 0;

            for (PlannedMatch plannedMatch: tournamentMatch.getPlannedMatches()) {
                Match match = plannedMatch.getExecutedMatch();
                if (match == null || match.getFinished() == null) {
                    return tournamentMatch;
                }
            }

            for (PlannedMatch plannedMatch: tournamentMatch.getPlannedMatches()) {
                Match match = plannedMatch.getExecutedMatch();
                if (match.getWinner() == null) {
                    draws++;
                } else if (tournamentMatch.getTeam1().equals(match.getWinner())) {
                    team1Wins++;
                } else if (tournamentMatch.getTeam2().equals(match.getWinner())) {
                    team2Wins++;
                }
            }

            tournamentMatch.setTeam1Score((team1Wins * gameConfig.getTournament().getPointsForWin()) + (draws * gameConfig.getTournament().getPointsForDraw()));
            tournamentMatch.setTeam2Score((team2Wins * gameConfig.getTournament().getPointsForWin()) + (draws * gameConfig.getTournament().getPointsForDraw()));
            tournamentMatch.setExecuted(true);
            return saveTournamentMatch(tournamentMatch);
        }
        return tournamentMatch;
    }
}
