package com.cowboy.shooter.service.tournament;

import com.cowboy.shooter.base.BooleanUtils;
import com.cowboy.shooter.base.MapUtils;
import com.cowboy.shooter.config.GameConfig;
import com.cowboy.shooter.exception.TournamentException;
import com.cowboy.shooter.model.*;
import com.cowboy.shooter.repository.TournamentTierRepository;
import com.cowboy.shooter.service.MatchExecutingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class TournamentTierService {

    @Autowired
    private TournamentTierRepository tournamentTierRepository;

    @Autowired
    private MatchExecutingService matchExecutingService;

    @Autowired
    private PlannedMatchService plannedMatchService;

    @Autowired
    private TournamentMatchService tournamentMatchService;

    @Autowired
    private GameConfig gameConfig;

    @Transactional(readOnly = true)
    public Optional<TournamentTier> getTier(String id) {
        return tournamentTierRepository.findById(id);
    }

    @Transactional
    public TournamentTier saveTier(TournamentTier tier) {
        return tournamentTierRepository.save(tier);
    }

    public void executeTier(String id) {
        Optional<TournamentTier> optionalTier = getTier(id);
        if (optionalTier.isPresent()) {
            TournamentTier tier = optionalTier.get();

            if (BooleanUtils.getBooleanValue(tier.getExecuted())) {
                throw new TournamentException("Tier [" + id + "] was already executed");
            }

            matchExecutingService.executeTier(tier);
            saveTier(tier);
        } else {
            throw new TournamentException("Tier id [" + id + "] does not exist");
        }
    }

    public TournamentTier createFirstTier(List<Team> teams) {
        TournamentTier tier = createTableTier(teams, gameConfig.getTournament().getFirstTierMatchesCount());
        return saveTier(tier);
    }

    public TournamentTier createSecondTier(TournamentTier firstTier) {
        return createSecondTier(new ArrayList<>(firstTier.getTeams()));
    }

    public TournamentTier createSecondTier(List<Team> teams) {
        return saveTier(createTableTier(teams, gameConfig.getTournament().getSecondTierMatchesCount()));
    }


    public TournamentTier createThirdTier(TournamentTier firstTier, TournamentTier secondTier) {
        List<TournamentMatch> allMatches = new ArrayList<>();
        allMatches.addAll(firstTier.getTournamentMatches());
        allMatches.addAll(secondTier.getTournamentMatches());

        Map<Team, Integer> teamScore = calculateScore(allMatches);

        List<Team> teams = teamScore.entrySet()
                .stream()
                .sorted((Map.Entry.<Team, Integer>comparingByValue().reversed()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
        return createThirdTier(teams);
    }

    public TournamentTier createThirdTier(List<Team> teams) {
        TournamentTier tier = createSpiderTier(Arrays.asList(
                teams.get(0),
                teams.get(1),
                teams.get(2),
                teams.get(3)
        ), gameConfig.getTournament().getThirdTierMatchesCount());
        return saveTier(tier);
    }

    public TournamentTier createFourthTier(TournamentTier thirdTier) {
        Map<Team, Integer> teamScoreFirstGroup = calculateScore(Arrays.asList(thirdTier.getTournamentMatches().get(0)));
        Map<Team, Integer> teamScoreSecondGroup = calculateScore(Arrays.asList(thirdTier.getTournamentMatches().get(1)));

        List<Team> teamsFirstGroup = teamScoreFirstGroup.entrySet()
                .stream()
                .sorted((Map.Entry.<Team, Integer>comparingByValue().reversed()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        List<Team> teamsSecondGroup = teamScoreSecondGroup.entrySet()
                .stream()
                .sorted((Map.Entry.<Team, Integer>comparingByValue().reversed()))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());


        return createFourthTier(Arrays.asList(teamsFirstGroup.get(0), teamsSecondGroup.get(0)));
    }

    public TournamentTier createFourthTier(List<Team> teams) {
        TournamentTier tier = createSpiderTier(Arrays.asList(
                teams.get(0),
                teams.get(1)
        ), gameConfig.getTournament().getFourthTierMatchesCount());
        return saveTier(tier);
    }

    public Map<Team, Integer> calculateScore(List<TournamentMatch> tournamentMatches) {
        Map<Team, Integer> teamScore = new LinkedHashMap<>();

        for (TournamentMatch tournamentMatch: tournamentMatches) {
            Team team1 = tournamentMatch.getTeam1();
            Team team2 = tournamentMatch.getTeam2();

            if (teamScore.containsKey(team1)) {
                teamScore.put(team1, teamScore.get(team1) + tournamentMatch.getTeam1Score());
            } else {
                teamScore.put(team1, tournamentMatch.getTeam1Score());
            }

            if (teamScore.containsKey(team2)) {
                teamScore.put(team2, teamScore.get(team2) + tournamentMatch.getTeam2Score());
            } else {
                teamScore.put(team2, tournamentMatch.getTeam2Score());
            }
        }

        return teamScore;
    }

    public TournamentTier createTableTier(List<Team> teams, int matchesCount) {
        TournamentTier tier = new TournamentTier();
        tier.setTeams(teams);

        for (int i = 0; i < teams.size(); i++) {
            for (int j = i + 1; j < teams.size(); j++) {
                Team team1 = teams.get(i);
                Team team2 = teams.get(j);

                TournamentMatch tournamentMatch = new TournamentMatch();
                tournamentMatch.getPlannedMatches().addAll(plannedMatchService.createPlannedMatches(team1, team2, matchesCount));
                tournamentMatch.setTeam1(team1);
                tournamentMatch.setTeam2(team2);
                tournamentMatchService.saveTournamentMatch(tournamentMatch);
                tier.getTournamentMatches().add(tournamentMatch);
            }
        }

        return tier;
    }

    public TournamentTier createSpiderTier(List<Team> teams, int matchesCount) {
        TournamentTier tier = new TournamentTier();
        tier.setTeams(teams);

        int teamsSize = teams.size();

        //Is teams size power of 2
        if (!(teamsSize > 1 && ((teamsSize & (teamsSize - 1)) == 0))) {
            throw new TournamentException("Teams in spider tier must be of size that is power of 2");
        }

        for (int i = 0; i < teamsSize; i += 2 ) {
            Team team1 = teams.get(i);
            Team team2 = teams.get(i + 1);

            TournamentMatch tournamentMatch = new TournamentMatch();
            tournamentMatch.getPlannedMatches().addAll(plannedMatchService.createPlannedMatches(team1, team2, matchesCount));
            tournamentMatch.setTeam1(team1);
            tournamentMatch.setTeam2(team2);
            tournamentMatchService.saveTournamentMatch(tournamentMatch);
            tier.getTournamentMatches().add(tournamentMatch);
        }

        return tier;
    }

}
