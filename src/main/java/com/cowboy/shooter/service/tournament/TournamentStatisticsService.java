package com.cowboy.shooter.service.tournament;

import com.cowboy.shooter.base.MapUtils;
import com.cowboy.shooter.dto.TeamMoveType;
import com.cowboy.shooter.dto.TournamentStatisticsDTO;
import com.cowboy.shooter.model.*;
import org.springframework.stereotype.Service;

@Service
public class TournamentStatisticsService {

    public TournamentStatisticsDTO getStatistics(Tournament tournament) {
        TournamentStatisticsDTO stats = new TournamentStatisticsDTO();
        if (tournament.getFirstTier() != null) {
            assignTierStats(stats, tournament.getFirstTier());
        }
        if (tournament.getSecondTier() != null) {
            assignTierStats(stats, tournament.getSecondTier());
        }
        if (tournament.getThirdTier() != null) {
            assignTierStats(stats, tournament.getThirdTier());
        }
        if (tournament.getFourthTier() != null) {
            assignTierStats(stats, tournament.getFourthTier());
        }

        return stats;
    }

    private void assignTierStats(TournamentStatisticsDTO stats, TournamentTier tier) {
        for(TournamentMatch tournamentMatch: tier.getTournamentMatches()) {
            if (tournamentMatch.getTeam1Score() != null) {
                MapUtils.addToMap(stats.getScore(), tournamentMatch.getTeam1().getName(), tournamentMatch.getTeam1Score());
            }
            if (tournamentMatch.getTeam2Score() != null) {
                MapUtils.addToMap(stats.getScore(), tournamentMatch.getTeam2().getName(), tournamentMatch.getTeam2Score());
            }
            for (PlannedMatch plannedMatch: tournamentMatch.getPlannedMatches()) {
                Match match = plannedMatch.getExecutedMatch();

                if (match == null) {
                    continue;
                }

                for (MatchMove move: match.getMoves()) {
                    assignMove(stats, match.getTeam1().getName(), move.getTeam1Move(), move.getTeam2Move());
                    assignMove(stats, match.getTeam2().getName(), move.getTeam2Move(), move.getTeam1Move());
                }

                if (match.getWinner() != null) {
                    MapUtils.addToMap(stats.getWins(), match.getWinner().getName());
                    if (match.getWinner().getId().equals(match.getTeam1().getId())) {
                        MapUtils.addToMap(stats.getLoses(), match.getTeam2().getName());
                    } else {
                        MapUtils.addToMap(stats.getLoses(), match.getTeam1().getName());
                    }
                } else {
                    MapUtils.addToMap(stats.getDraws(), match.getTeam1().getName());
                    MapUtils.addToMap(stats.getDraws(), match.getTeam2().getName());
                }
            }
        }
    }

    private void assignMove(TournamentStatisticsDTO stats, String teamName, TeamMove move, TeamMove otherTeamMove) {
        switch (move.getTeamMove()) {
            case BLOCK:
                if (move.isFaulty()) {
                    MapUtils.addToMap(stats.getFailResponses(), teamName);
                    MapUtils.addToMap(stats.getUnsuccessfulBlocks(), teamName);
                } else {
                    MapUtils.addToMap(stats.getBlocks(), teamName);
                    if (TeamMoveType.SHOOT.equals(otherTeamMove.getTeamMove()) && !otherTeamMove.isFaulty()) {
                        MapUtils.addToMap(stats.getBlockedShots(), teamName);
                    }
                }
                break;
            case SHOOT:
                if (move.isFaulty()) {
                    MapUtils.addToMap(stats.getFailResponses(), teamName);
                    MapUtils.addToMap(stats.getUnsuccessfulShots(), teamName);
                } else {
                    MapUtils.addToMap(stats.getBulletsShot(), teamName);
                }
                break;
            case EVADE:
                if (move.isFaulty()) {
                    MapUtils.addToMap(stats.getFailResponses(), teamName);
                    MapUtils.addToMap(stats.getUnsuccessfulEvades(), teamName);
                } else {
                    MapUtils.addToMap(stats.getEvades(), teamName);
                    if (TeamMoveType.SHOOT.equals(otherTeamMove.getTeamMove()) && !otherTeamMove.isFaulty()) {
                        MapUtils.addToMap(stats.getEvadedShots(), teamName);
                    }
                }
                break;
            case RELOAD:
                if (move.isFaulty()) {
                    MapUtils.addToMap(stats.getFailResponses(), teamName);
                    MapUtils.addToMap(stats.getUnsuccessfulReloads(), teamName);
                } else {
                    MapUtils.addToMap(stats.getReloads(), teamName);
                }
                break;
            case NULL:
                MapUtils.addToMap(stats.getFailResponses(), teamName);
                break;
        }
    }

}
