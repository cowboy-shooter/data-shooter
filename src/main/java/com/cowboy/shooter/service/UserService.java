package com.cowboy.shooter.service;

import com.cowboy.shooter.config.KeycloakSecurityConfig;
import com.cowboy.shooter.dto.PageableResponse;
import com.cowboy.shooter.model.User;
import com.cowboy.shooter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private KeycloakSecurityConfig keycloakSecurityConfig;

    @Transactional
    public User createUser(String id, String email, String firstName, String lastName, String username) {
        Optional<User> optionalUser = getUser(id);
        User user = null;
        if (optionalUser.isPresent()) {
            user = optionalUser.get();
        } else {
            user = new User();
            user.setId(id);
        }
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setUsername(username);
        return this.userRepository.save(user);
    }

    @Transactional
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Transactional(readOnly = true)
    public PageableResponse<User> getAllUsers(Pageable pageable) {
        return new PageableResponse(userRepository.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public PageableResponse<User> getAllUsersWithoutTeam(Pageable pageable) {
        return new PageableResponse(this.userRepository.findAllByTeamIsNullAndIdNot(pageable, getOwnUser().getId()));
    }

    @Transactional(readOnly = true)
    public Optional<User> getUser(String id) {
        return this.userRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public User getOwnUser() {
        return getUser(keycloakSecurityConfig.getAccessToken().getSubject()).get();
    }

    @Transactional
    public void leaveTeam(User user) {
        user.setTeam(null);
        userRepository.save(user);
    }

}
