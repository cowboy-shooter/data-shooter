package com.cowboy.shooter.service;

import com.cowboy.shooter.dto.PageableResponse;
import com.cowboy.shooter.dto.TeamDTO;
import com.cowboy.shooter.dto.type.ProgrammingLanguagesType;
import com.cowboy.shooter.exception.ValidationException;
import com.cowboy.shooter.model.Team;
import com.cowboy.shooter.model.User;
import com.cowboy.shooter.repository.TeamRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Transactional
    public Team createTeam(String name, ProgrammingLanguagesType language, String description, String url, String flag) {
        Optional<Team> existingTeam = teamRepository.findByName(name);
        if (existingTeam.isPresent()) {
            throw new ValidationException("Team with that name already exists");
        }
        return saveTeam(new Team(), name, language, description, url, flag);
    }

    @Transactional
    public Team updateTeam(String id, String name, ProgrammingLanguagesType language, String description, String url, String flag) {
        Optional<Team> existingTeam = teamRepository.findById(id);
        if (!existingTeam.isPresent()) {
            throw new ValidationException("Team with that id does not exist");
        }
        return saveTeam(existingTeam.get(), name, language, description, url, flag);
    }

    @Transactional
    public Team saveTeam(Team team, String name, ProgrammingLanguagesType language, String description, String url, String flag) {
        team.setName(name);
        team.setUrl(url);
        team.setDescription(description);
        team.setLanguage(language);
        team.setFlag(flag);

        return teamRepository.save(team);
    }

    @Transactional(readOnly = true)
    public PageableResponse<Team> getAllTeams(Pageable pageable) {
        return new PageableResponse(teamRepository.findAll(pageable));
    }

    @Transactional(readOnly = true)
    public List<Team> getAllTeams() {
        return teamRepository.findAll();
    }

    @Transactional
    public Team saveTeam(Team team) {
        return teamRepository.save(team);
    }

    @Transactional(readOnly = true)
    public Optional<Team> getTeamWithMember(User member) {
        return teamRepository.findByMembersContains(member);
    }

    @Transactional(readOnly = true)
    public Optional<Team> getTeam(String id) {
        return teamRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public List<Team> getTeams(List<String> teamIds) {
        return teamRepository.findAllById(teamIds);
    }

    @Transactional(readOnly = true)
    public Optional<Team> getTeamWithName(String name) {
        return teamRepository.findByName(name);
    }

    @Transactional(readOnly = true)
    public Optional<Team> getTeamWithUrl(String url) {
        return teamRepository.findByUrl(url);
    }

    @Transactional(readOnly = true)
    public TeamDTO getTeamDto(String id) {
        return modelMapper.map(getTeam(id).get(), TeamDTO.class);
    }

    @Transactional
    public List<Team> getInvites(User user) {
        return teamRepository.findByInviteesContains(user);
    }

    @Transactional
    public void makeMemberLeader(User member, Team team) {
        team.setLeader(member);
        teamRepository.save(team);
    }

    @Transactional
    public void deleteTeam(Team team) {
        teamRepository.delete(team);
    }

    @Transactional
    public List<Team> getChallengers(Team team) {
        return teamRepository.findByChallengedTeamsContains(team);
    }

}
