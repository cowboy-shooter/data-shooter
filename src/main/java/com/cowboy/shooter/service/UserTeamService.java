package com.cowboy.shooter.service;

import com.cowboy.shooter.base.StringUtils;
import com.cowboy.shooter.dto.PageableResponse;
import com.cowboy.shooter.dto.type.ProgrammingLanguagesType;
import com.cowboy.shooter.dto.type.TeamChallengesType;
import com.cowboy.shooter.dto.type.TeamUserManagementType;
import com.cowboy.shooter.dto.wrapper.TeamChallengesManagementWrapper;
import com.cowboy.shooter.dto.wrapper.TeamUserManagementWrapper;
import com.cowboy.shooter.exception.ValidationException;
import com.cowboy.shooter.model.Team;
import com.cowboy.shooter.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserTeamService {

    @Autowired
    private TeamService teamService;

    @Autowired
    private UserService userService;

    public Team createTeam(String name, ProgrammingLanguagesType language, String description, String url, String flag) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(flag)) {
            throw new ValidationException("Name or Flag were empty");
        }
        Team team = teamService.createTeam(name, language, description, url, flag);
        User user = userService.getOwnUser();
        joinTeam(user.getId(), team.getId());
        makeMemberLeader(user.getId(), team.getId());
        return team;
    }

    public Team updateTeam(String name, ProgrammingLanguagesType language, String description, String url, String flag) {
        if (StringUtils.isEmpty(name) || StringUtils.isEmpty(flag)) {
            throw new ValidationException("Name or Flag were empty");
        }
        Team ownTeam = getOwnTeam();
        Team team = teamService.updateTeam(ownTeam.getId(), name, language, description, url, flag);
        return team;
    }

    public void inviteMember(String userId) {
        Team team = getOwnTeam();
        User user = userService.getUser(userId).get();
        if (!team.getInvitees().contains(user)) {
            team.getInvitees().add(user);
            teamService.saveTeam(team);
        }
    }

    public void cancelInvite(String userId) {
        Team team = getOwnTeam();
        User user = userService.getUser(userId).get();
        if (team.getInvitees().contains(user)) {
            team.getInvitees().remove(user);
            teamService.saveTeam(team);
        }
    }

    public void joinTeam(String userId, String teamId) {
        Optional<User> optionalUser = userService.getUser(userId);
        if (optionalUser.isPresent()) {
            Team team = teamService.getTeam(teamId).get();
            if (team.getMembers().size() < 3) {
                User user = optionalUser.get();
                user.setTeam(team);
                userService.saveUser(user);
                team = teamService.getTeam(teamId).get();
                team.getInvitees().remove(optionalUser.get());
                team.getRequestors().remove(optionalUser.get());
                teamService.saveTeam(team);
            }
        }
    }

    public void requestJoining(String teamId) {
        User user = userService.getOwnUser();
        Team team = teamService.getTeam(teamId).get();
        if (!team.getRequestors().contains(user)) {
            team.getRequestors().add(user);
            teamService.saveTeam(team);
        }
    }

    public void cancelRequestJoining(String teamId) {
        User user = userService.getOwnUser();
        Team team = teamService.getTeam(teamId).get();
        if (team.getRequestors().contains(user)) {
            team.getRequestors().remove(user);
            teamService.saveTeam(team);
        }
    }

    public void acceptRequest(String userId) {
        User user = userService.getUser(userId).get();
        Team team = getOwnTeam();
        if (team.getRequestors().contains(user)) {
            team.getRequestors().remove(user);
            teamService.saveTeam(team);
            user.setTeam(team);
            joinTeam(userId, team.getId());
        }
    }

    public void acceptRequestJoining(String teamId) {
        User user = userService.getOwnUser();
        Team team = teamService.getTeam(teamId).get();
        if (team.getInvitees().contains(user)) {
            team.getInvitees().remove(user);
            teamService.saveTeam(team);
        }
        user.setTeam(team);
        userService.saveUser(user);
    }

    public Team getOwnTeam() {
        Optional<Team> team = teamService.getTeamWithMember(userService.getOwnUser());
        if (team.isPresent()) {
            return team.get();
        } else {
            return null;
        }
    }

    public void makeMemberLeader(String userId) {
        Optional<User> optionalUser = userService.getUser(userId);
        if (optionalUser.isPresent()) {
            Team team = getOwnTeam();
            makeMemberLeader(optionalUser.get().getId(), team.getId());
        }
    }

    public void makeMemberLeader(String userId, String teamId) {
        Optional<User> optionalUser = userService.getUser(userId);
        if (optionalUser.isPresent()) {
            Team team = teamService.getTeam(teamId).get();
            User user = optionalUser.get();
            teamService.makeMemberLeader(user, team);
        }
    }

    public void removeMember(String userId) {
        removeMember(userService.getUser(userId).get());
    }

    public void leaveTeam() {
        removeMember(userService.getOwnUser());
    }

    public void removeMember(User user) {
        String teamId = user.getTeam().getId();
        userService.leaveTeam(user);
        Team team = teamService.getTeam(teamId).get();
        if (team.getMembers().isEmpty()) {
            teamService.deleteTeam(team);
        } else {
            makeMemberLeader(team.getMembers().get(0).getId(), teamId);
        }
    }

    public List<Team> getInvites() {
        return teamService.getInvites(userService.getOwnUser());
    }

    public PageableResponse getOwnTeamManagement(Pageable pageable) {
        Team team = getOwnTeam();

        PageableResponse<User> users = userService.getAllUsersWithoutTeam(pageable);

        List<TeamUserManagementWrapper> wrappedUsers =
                users.getContent()
                        .stream()
                        .map((user) -> {
                            if (team.getInvitees().contains(user)) {
                                return new TeamUserManagementWrapper(user, TeamUserManagementType.INVITED);
                            } else if (team.getRequestors().contains(user)) {
                                return new TeamUserManagementWrapper(user, TeamUserManagementType.REQUESTOR);
                            } else {
                                return new TeamUserManagementWrapper(user, TeamUserManagementType.NONE);
                            }
                        }).collect(Collectors.toList());

        PageableResponse<TeamUserManagementWrapper> wrappedUsersPage = new PageableResponse();
        wrappedUsersPage.setPageInfo(users.getPageInfo());
        wrappedUsersPage.setContent(wrappedUsers);
        return wrappedUsersPage;
    }

    public PageableResponse getOwnTeamChallengesManagement(Pageable pageable) {
        Team ownTeam = getOwnTeam();
        PageableResponse<Team> teams = teamService.getAllTeams(pageable);

        List<TeamChallengesManagementWrapper> wrappedTeams =
                teams.getContent()
                        .stream()
                        .map((team) -> {
                            for (Team challengedTeam: ownTeam.getChallengedTeams()) {
                                if (challengedTeam.getId().equals(team.getId())) {
                                    return new TeamChallengesManagementWrapper(team, TeamChallengesType.CHALLENGED);
                                }
                            }
                            return new TeamChallengesManagementWrapper(team, TeamChallengesType.NONE);
                        }).collect(Collectors.toList());

        PageableResponse<TeamChallengesManagementWrapper> wrappedTeamPage = new PageableResponse();
        wrappedTeamPage.setPageInfo(teams.getPageInfo());
        wrappedTeamPage.setContent(wrappedTeams);
        return wrappedTeamPage;
    }

    public void changeFlag(String flag) {
        if (StringUtils.isEmpty(flag)) {
            throw new ValidationException("Flag was empty");
        }
        Team team = getOwnTeam();
        team.setFlag(flag);
        teamService.saveTeam(team);
    }

    public void challengeTeam(String teamId) {
        Team ownTeam = getOwnTeam();
        Team challenged = teamService.getTeam(teamId).get();
        if (!ownTeam.getChallengedTeams().contains(challenged)) {
            ownTeam.getChallengedTeams().add(challenged);
        }
        teamService.saveTeam(ownTeam);
    }

    public boolean cancelChallenge(String teamId) {
        Team ownTeam = getOwnTeam();
        Team challenged = teamService.getTeam(teamId).get();
        boolean wasChallenged = false;
        for (Team team: ownTeam.getChallengedTeams()) {
            if (team.getId().equals(challenged.getId())) {
                wasChallenged = true;
                break;
            }
        }
        if (wasChallenged) {
            ownTeam.getChallengedTeams().remove(challenged);
        }
        teamService.saveTeam(ownTeam);
        return wasChallenged;
    }

    public boolean acceptChallenge(String teamId) {
        Team ownTeam = getOwnTeam();
        List<Team> challengers = getChallengers();
        challengers = challengers.stream()
                .filter(challenger -> challenger.getId().equals(teamId))
                .collect(Collectors.toList());
        Team challenger = null;
        if (challengers != null && challengers.size() > 0) {
            challenger = challengers.get(0);
        }

        if (challenger == null) {
            return false;
        }

        boolean wasChallenged = false;
        for (Team team: challenger.getChallengedTeams()) {
            if (team.getId().equals(ownTeam.getId())) {
                wasChallenged = true;
                break;
            }
        }
        if (wasChallenged) {
            challenger.getChallengedTeams().remove(ownTeam);
        }
        teamService.saveTeam(challenger);
        return wasChallenged;
    }

    public List<Team> getChallengers() {
        Team ownTeam = getOwnTeam();
        return teamService.getChallengers(ownTeam);
    }
}
