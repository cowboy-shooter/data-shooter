package com.cowboy.shooter;

import com.cowboy.shooter.repository.TeamRepository;
import com.cowboy.shooter.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class Application extends SpringBootServletInitializer {

    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private UserRepository userRepository;

    public static void main(String[] args){
        SpringApplication.run(Application.class, args);
    }

//    @PostConstruct
//    public void initData() throws IOException {
//        User user1 = new User();
//        user1.setId("98f46f3e-8762-41c7-819e-487c568f9a58");
//        user1.setFirstName("Marcel");
//        user1.setLastName("Ouška");
//        user1.setEmail("marcel.ouska@gmail.com");
//        user1.setUsername("marcel.ouska@gmail.com");
//
//        User user2 = new User();
//        user2.setId("BBB");
//        user2.setFirstName("Jáchym");
//        user2.setLastName("Košata");
//        user2.setEmail("jachym.kosata@gmail.com");
//        user2.setUsername("jachym.kosata@gmail.com");
//
//        User user3 = new User();
//        user3.setId("CCC");
//        user3.setFirstName("Dominik");
//        user3.setLastName("Potočný");
//        user3.setEmail("dominik.potocny@gmail.com");
//        user3.setUsername("dominik.potocny@gmail.com");
//
//        User user4 = new User();
//        user4.setId("DDD");
//        user4.setFirstName("Michal");
//        user4.setLastName("Valach");
//        user4.setEmail("michal.valach@gmail.com");
//        user4.setUsername("michal.valach@gmail.com");
//
//        User user5 = new User();
//        user5.setId("EEE");
//        user5.setFirstName("Marcel");
//        user5.setLastName("Sabol");
//        user5.setEmail("marcel.sabol@gmail.com");
//        user5.setUsername("marcel.sabol@gmail.com");
//
//        User user6 = new User();
//        user6.setId("FFF");
//        user6.setFirstName("Jan");
//        user6.setLastName("Vrátník");
//        user6.setEmail("jan.vratnik@gmail.com");
//        user6.setUsername("jan.vratnik@gmail.com");
//
//        User user7 = new User();
//        user7.setId("GGG");
//        user7.setFirstName("Stanislav");
//        user7.setLastName("Šimek");
//        user7.setEmail("stanislav.simek@gmail.com");
//        user7.setUsername("stanislav.simek@gmail.com");
//
//        User user8 = new User();
//        user8.setId("HHH");
//        user8.setFirstName("Martin");
//        user8.setLastName("Slavík");
//        user8.setEmail("martin.slavik@gmail.com");
//        user8.setUsername("martin.slavik@gmail.com");
//
//        User user9 = new User();
//        user9.setId("III");
//        user9.setFirstName("Vojtěch");
//        user9.setLastName("Krákora");
//        user9.setEmail("vojtech.krakora@gmail.com");
//        user9.setUsername("vojtech.krakora@gmail.com");
//
//        User user10 = new User();
//        user10.setId("JJJ");
//        user10.setFirstName("Jaromír");
//        user10.setLastName("Vrátný");
//        user10.setEmail("jidash.v.rimmer@gmail.com");
//        user10.setUsername("jidash.v.rimmer@gmail.com");
//
//        User user11 = new User();
//        user11.setId("KKK");
//        user11.setFirstName("Tomáš");
//        user11.setLastName("Slabý");
//        user11.setEmail("tomas.slaby@gmail.com");
//        user11.setUsername("tomas.slaby@gmail.com");
//
//        Team team1 = new Team();
//        team1.setName("TeamA");
//        team1.setFlag(encodeImg("/mock/flags/flag1.png"));
//
//        Team team2 = new Team();
//        team2.setName("TeamB");
//        team2.setFlag(encodeImg("/mock/flags/flag2.png"));
//
//        team1.setLeader(user1);
//        team2.setLeader(user5);
//
//        user1 = userRepository.save(user1);
//        user2 = userRepository.save(user2);
//        user3 = userRepository.save(user3);
//        user4 = userRepository.save(user4);
//        user5 = userRepository.save(user5);
//        user6 = userRepository.save(user6);
//        user7 = userRepository.save(user7);
//        user8 = userRepository.save(user8);
//        user9 = userRepository.save(user9);
//        user10 = userRepository.save(user10);
//        user11 = userRepository.save(user11);
//
//        team1 = teamRepository.save(team1);
//        team2 = teamRepository.save(team2);
//
//        user1.setTeam(team1);
//        user2.setTeam(team1);
//        user3.setTeam(team2);
//        user4.setTeam(team2);
//        user5.setTeam(team2);
//
//
//        user1 = userRepository.save(user1);
//        user2 = userRepository.save(user2);
//        user3 = userRepository.save(user3);
//        user4 = userRepository.save(user4);
//        user5 = userRepository.save(user5);
//        user6 = userRepository.save(user6);
//        user7 = userRepository.save(user7);
//        user8 = userRepository.save(user8);
//    }
//
//    private String encodeImg(String path) throws IOException {
//        File file = new File(Application.class.getResource(path).getFile());
//        InputStream finput = new FileInputStream(file);
//        byte[] imageBytes = new byte[(int)file.length()];
//        finput.read(imageBytes, 0, imageBytes.length);
//        finput.close();
//        return "data:image/png;base64," + Base64.getEncoder().encodeToString(imageBytes);
//    }

}
