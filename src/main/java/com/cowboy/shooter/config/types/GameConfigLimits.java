package com.cowboy.shooter.config.types;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GameConfigLimits {

    private int maximumBlocksInRow;
    private int maximumEvades;
    private int maximumBullets;
    private int maximumRounds;

}
