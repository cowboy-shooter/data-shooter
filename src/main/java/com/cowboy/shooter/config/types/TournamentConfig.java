package com.cowboy.shooter.config.types;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TournamentConfig {

    private int firstTierMatchesCount;
    private int secondTierMatchesCount;
    private int thirdTierMatchesCount;
    private int fourthTierMatchesCount;
    private int pointsForWin;
    private int pointsForDraw;
}
