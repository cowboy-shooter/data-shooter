package com.cowboy.shooter.config;

import com.cowboy.shooter.config.types.GameConfigLimits;
import com.cowboy.shooter.config.types.TournamentConfig;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "shooter.game")
public class GameConfig {

    private GameConfigLimits limits;
    private TournamentConfig tournament;

}
