package com.cowboy.shooter.rest;

import com.cowboy.shooter.dto.MatchMoveRequestPayload;
import com.cowboy.shooter.dto.TeamDTO;
import com.cowboy.shooter.dto.TeamMoveRestResponse;
import com.cowboy.shooter.exception.RestException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@Service
public class TeamMoveController {

    @Autowired
    private RestTemplate restTemplate;

    public TeamMoveRestResponse getMove(MatchMoveRequestPayload request, TeamDTO team) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<MatchMoveRequestPayload> entity = new HttpEntity<>(request, headers);

        return restTemplate.exchange(team.getUrl(), HttpMethod.POST, entity, TeamMoveRestResponse.class).getBody();
    }

}
