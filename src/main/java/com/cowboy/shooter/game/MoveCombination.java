package com.cowboy.shooter.game;

import com.cowboy.shooter.dto.TeamGameObject;
import com.cowboy.shooter.dto.TeamMoveType;
import com.cowboy.shooter.dto.TeamMoveWrapper;
import com.cowboy.shooter.model.TeamMove;

import java.sql.Wrapper;
import java.util.Arrays;
import java.util.stream.Collectors;

public enum MoveCombination {
    NULL_NULL(TeamMoveType.NULL, TeamMoveType.NULL) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, false, false);

            return wrapper;
        }
    },
    SHOOT_NULL(TeamMoveType.SHOOT, TeamMoveType.NULL) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canShoot(), false);
            if (team1.canShoot()) {
                team1.shoot();
                team2.setIsDead(true);
            }

            return wrapper;
        }
    },
    BLOCK_NULL(TeamMoveType.BLOCK, TeamMoveType.NULL) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canBlock(), false);
            if (team1.canBlock()) {
                team1.block();
            }

            return wrapper;
        }
    },
    EVADE_NULL(TeamMoveType.EVADE, TeamMoveType.NULL) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canEvade(), false);

            if (team1.canEvade()) {
                team1.evade();
            }

            return wrapper;
        }
    },
    RELOAD_NULL(TeamMoveType.RELOAD, TeamMoveType.NULL) {

        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canReload(), false);
            if (team1.canReload()) {
                team1.reload();
            }

            return wrapper;
        }
    },
    SHOOT_SHOOT(TeamMoveType.SHOOT, TeamMoveType.SHOOT) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canShoot(), team2.canShoot());
            if (team1.canShoot()) {
                team1.shoot();
                if (team2.canShoot()) {
                    team2.shoot();
                } else {
                    team2.setIsDead(true);
                }
            } else if (team2.canShoot()) {
                team2.shoot();
                if (team1.canShoot()) {
                    team1.shoot();
                } else {
                    team1.setIsDead(true);
                }
            }

            return wrapper;
        }
    },
    SHOOT_RELOAD(TeamMoveType.SHOOT, TeamMoveType.RELOAD) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canShoot(), team2.canReload());
            if (team1.canShoot()) {
                team1.shoot();
                team2.setIsDead(true);
            } else {
                team2.reload();
            }

            return wrapper;
        }
    },
    SHOOT_BLOCK(TeamMoveType.SHOOT, TeamMoveType.BLOCK) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canShoot(), team2.canBlock());
            if (team1.canShoot()) {
                team1.shoot();
                if (team2.canBlock()) {
                    team2.block();
                } else {
                    team2.setIsDead(true);
                }
            } else {
                team2.block();
            }

            return wrapper;
        }
    },
    SHOOT_EVADE(TeamMoveType.SHOOT, TeamMoveType.EVADE) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canShoot(), team2.canEvade());
            if (team1.canShoot()) {
                team1.shoot();
                if (team2.canEvade()) {
                    team2.evade();
                } else {
                    team2.setIsDead(true);
                }
            } else {
                team2.evade();
            }

            return wrapper;
        }
    },
    RELOAD_RELOAD(TeamMoveType.RELOAD, TeamMoveType.RELOAD) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canReload(), team2.canReload());
            team1.reload();
            team2.reload();

            return wrapper;
        }
    },
    RELOAD_BLOCK(TeamMoveType.RELOAD, TeamMoveType.BLOCK) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canReload(), team2.canBlock());
            team1.reload();
            team2.block();

            return wrapper;
        }
    },
    RELOAD_EVADE(TeamMoveType.RELOAD, TeamMoveType.EVADE) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canReload(), team2.canEvade());
            team1.reload();
            team2.evade();

            return wrapper;
        }
    },
    BLOCK_BLOCK(TeamMoveType.BLOCK, TeamMoveType.BLOCK) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canBlock(), team2.canBlock());
            team1.block();
            team2.block();

            return wrapper;
        }
    },
    BLOCK_EVADE(TeamMoveType.BLOCK, TeamMoveType.EVADE) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canBlock(), team2.canEvade());
            team1.block();
            team2.evade();

            return wrapper;
        }
    },
    EVADE_EVADE(TeamMoveType.EVADE, TeamMoveType.EVADE) {
        @Override
        public TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2) {
            TeamMoveWrapper wrapper = MoveCombination.getWrapper(this, team1.canEvade(), team2.canEvade());
            team1.evade();
            team2.evade();

            return wrapper;
        }
    };

    TeamMoveType move1;
    TeamMoveType move2;

    MoveCombination(TeamMoveType move1, TeamMoveType move2) {
        this.move1 = move1;
        this.move2 = move2;
    }

    public abstract TeamMoveWrapper doAction(TeamGameObject team1, TeamGameObject team2);

    public static TeamMoveWrapper executeMove(TeamGameObject team1, TeamGameObject team2, TeamMoveType move1, TeamMoveType move2) {
        MoveCombination moveCombination = Arrays.stream(MoveCombination.values())
                .filter(combination -> (combination.move1.equals(move1) && combination.move2.equals(move2))
                        || combination.move1.equals(move2) && combination.move2.equals(move1))
                .collect(Collectors.toList())
                .get(0);

        if (moveCombination.move1.equals(move1)) {
            return moveCombination.doAction(team1, team2);
        } else {
            TeamMoveWrapper wrapper = moveCombination.doAction(team2, team1);
            TeamMove team1Move = wrapper.getTeam1Move();
            wrapper.setTeam1Move(wrapper.getTeam2Move());
            wrapper.setTeam2Move(team1Move);
            return wrapper;
        }
    }

    public static TeamMoveWrapper getWrapper(MoveCombination combination, boolean canPerform1, boolean canPerform2) {
        TeamMove team1Move = new TeamMove();
        TeamMove team2Move = new TeamMove();

        team1Move.setTeamMove(combination.move1);
        team2Move.setTeamMove(combination.move2);

        if (!canPerform1) {
            team1Move.setFaulty(true);
            team1Move.setMessage("This action is not possible");
        }
        if (!canPerform2) {
            team2Move.setFaulty(true);
            team2Move.setMessage("This action is not possible");
        }
        return new TeamMoveWrapper(team1Move, team2Move);
    }

}
