package com.cowboy.shooter.game;

import com.cowboy.shooter.base.StringUtils;
import com.cowboy.shooter.config.GameConfig;
import com.cowboy.shooter.dto.*;
import com.cowboy.shooter.model.MatchMove;
import com.cowboy.shooter.model.TeamMatchState;
import com.cowboy.shooter.rest.TeamMoveController;
import com.cowboy.shooter.service.MatchService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MatchExecutor {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private TeamMoveController controller;

    @Autowired
    private MatchService matchService;

    @Autowired
    private GameConfig gameConfig;

    @Async
    public void executeMatchAsync(String matchId, TeamDTO team1DTO, TeamDTO team2DTO) {
        executeMatch(matchId, team1DTO, team2DTO);
    }

    public void executeMatch(String matchId, TeamDTO team1DTO, TeamDTO team2DTO) {
        MatchMove latestMove = new MatchMove();
        TeamGameObject team1 = new TeamGameObject(team1DTO, gameConfig.getLimits());
        TeamGameObject team2 = new TeamGameObject(team2DTO, gameConfig.getLimits());

        for (int i = 0; i < gameConfig.getLimits().getMaximumRounds() && !team1.getIsDead() && !team2.getIsDead(); i++) {
            TeamMoveType team1MoveType;
            TeamMoveType team2MoveType;

            String team1Exception = "";
            String team2Exception = "";
            try {
                TeamMoveType type = null;
                if (latestMove.getTeam2Move() != null) {
                    type = latestMove.getTeam2Move().getTeamMove();
                }
                team1MoveType = controller.getMove(new MatchMoveRequestPayload(i, type, team2.getTeam().getId(), team2.getTeam().getName()), team1.getTeam()).getMove();
            } catch (Exception ex) {
                team1MoveType = TeamMoveType.NULL;
                team1Exception = ex.getMessage();
                // log exception
            }
            try {
                TeamMoveType type = null;
                if (latestMove.getTeam1Move() != null) {
                    type = latestMove.getTeam1Move().getTeamMove();
                }
                team2MoveType = controller.getMove(new MatchMoveRequestPayload(i, type, team1.getTeam().getId(), team1.getTeam().getName()), team2.getTeam()).getMove();
            } catch (Exception ex) {
                team2MoveType = TeamMoveType.NULL;
                team2Exception = ex.getMessage();
                // log exception
            }

            TeamMoveWrapper wrapper = MoveCombination.executeMove(team1, team2, team1MoveType, team2MoveType);

            if (!StringUtils.isEmpty(team1Exception)) {
                wrapper.getTeam1Move().setMessage(wrapper.getTeam1Move().getMessage() + " " + team1Exception);
            }
            if (!StringUtils.isEmpty(team2Exception)) {
                wrapper.getTeam2Move().setMessage(wrapper.getTeam2Move().getMessage() + " " + team2Exception);
            }

            MatchMove move = new MatchMove();
            move.setSequenceNumber(i);
            move.setTeam1Move(wrapper.getTeam1Move());
            move.setTeam2Move(wrapper.getTeam2Move());
            move.setTeam1State(modelMapper.map(team1, TeamMatchState.class));
            move.setTeam2State(modelMapper.map(team2, TeamMatchState.class));
            latestMove = move;

            matchService.addMove(matchId, move);
        }

        if (team1.getIsDead()) {
            matchService.finishMatch(matchId, team2.getTeam().getId());
        } else if (team2.getIsDead()) {
            matchService.finishMatch(matchId, team1.getTeam().getId());
        } else {
            matchService.finishMatch(matchId, null);
        }
    }

}
