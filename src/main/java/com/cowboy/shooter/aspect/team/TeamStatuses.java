package com.cowboy.shooter.aspect.team;

public enum TeamStatuses {
    HAS_NO_TEAM,
    HAS_TEAM,
    IS_LEADER_OF_TEAM
}
