package com.cowboy.shooter.aspect.team;

import com.cowboy.shooter.aspect.auth.role.HasRole;
import com.cowboy.shooter.aspect.auth.role.Roles;
import com.cowboy.shooter.config.DevelopmentConfig;
import com.cowboy.shooter.exception.NotAuthorizedException;
import com.cowboy.shooter.exception.TeamStatusException;
import com.cowboy.shooter.model.Team;
import com.cowboy.shooter.model.User;
import com.cowboy.shooter.service.UserService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

@Aspect
@Component
public class TeamStatusCheckAspect {

    @Autowired
    private DevelopmentConfig developmentConfig;

    @Autowired
    private UserService userService;

    @Around("@annotation(com.cowboy.shooter.aspect.team.TeamStatusCheck)")
    public Object checkTeamStatus(ProceedingJoinPoint joinPoint) throws Throwable {
        if (developmentConfig.isDisableAuthorization()) {
            return joinPoint.proceed();
        }

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        TeamStatusCheck statusCheck = method.getAnnotation(TeamStatusCheck.class);
        TeamStatuses status = statusCheck.value();

        User user = userService.getOwnUser();
        Team team = user.getTeam();

        switch (status) {
            case HAS_NO_TEAM:
                if (team != null) {
                    throw new TeamStatusException("User [" + user.getUsername() + "] already has a team.");
                }
                break;
            case HAS_TEAM:
                if (team == null) {
                    throw new TeamStatusException("User [" + user.getUsername() + "] has no team.");
                }
                break;
            case IS_LEADER_OF_TEAM:
                if (team == null || !team.getLeader().getId().equals(user.getId())) {
                    throw new TeamStatusException("User [" + user.getUsername() + "] has no team or is not a leader.");
                }
                break;
        }

        return joinPoint.proceed();
    }

}
