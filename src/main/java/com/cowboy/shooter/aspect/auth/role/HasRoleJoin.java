package com.cowboy.shooter.aspect.auth.role;

import org.keycloak.representations.AccessToken;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public enum HasRoleJoin {
    ONE_OF {
        @Override
        public boolean authorize(AccessToken accessToken, List<Roles> roles) {
            AtomicReference<Boolean> isAuthorized = new AtomicReference<>(false);
            roles.forEach(
                    role -> {
                        if (hasRole(role, accessToken)) {
                            isAuthorized.set(true);
                            return;
                        }
                    }
            );
            return isAuthorized.get();
        }
    }, ALL_OF {
        @Override
        public boolean authorize(AccessToken accessToken, List<Roles> roles) {
            AtomicReference<Boolean> isAuthorized = new AtomicReference<>(true);
            roles.forEach(
                    role -> {
                        if (!hasRole(role, accessToken)) {
                            isAuthorized.set(false);
                            return;
                        }
                    }
            );
            return isAuthorized.get();
        }
    };

    private static boolean hasRole(Roles role, AccessToken accessToken) {
        boolean hasRole = false;
        switch (role.getRoleType()) {
            case REALM:
                hasRole = accessToken.getRealmAccess().getRoles().contains(role.getName());
                break;
            case CLIENT:
                hasRole = accessToken.getResourceAccess(role.getResource()).getRoles().contains(role.getName());
                break;
        }
        return hasRole;
    }

    public abstract boolean authorize(AccessToken accessToken, List<Roles> roles);
}
