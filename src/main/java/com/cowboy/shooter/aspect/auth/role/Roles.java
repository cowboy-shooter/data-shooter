package com.cowboy.shooter.aspect.auth.role;

public enum Roles {
    ADMIN(RoleType.CLIENT, "gui-shooter", "admin"),
    REGULAR(RoleType.CLIENT, "gui-shooter", "regular");

    private RoleType roleType;
    private String resource;
    private String name;

    Roles(RoleType roleType, String resource, String name) {
        this.roleType = roleType;
        this.resource = resource;
        this.name = name;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public String getResource() {
        return resource;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Roles{" +
                "roleType=" + roleType +
                ", resource='" + resource + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
