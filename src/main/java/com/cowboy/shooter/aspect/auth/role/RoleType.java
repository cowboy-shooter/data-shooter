package com.cowboy.shooter.aspect.auth.role;

public enum RoleType {
    REALM,
    CLIENT
}
