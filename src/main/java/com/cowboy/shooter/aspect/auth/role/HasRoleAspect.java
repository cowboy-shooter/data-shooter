package com.cowboy.shooter.aspect.auth.role;

import com.cowboy.shooter.config.DevelopmentConfig;
import com.cowboy.shooter.config.KeycloakSecurityConfig;
import com.cowboy.shooter.exception.NotAuthorizedException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

@Aspect
@Component
public class HasRoleAspect {

    @Autowired
    private KeycloakSecurityConfig keycloakSecurityConfig;

    @Autowired
    private DevelopmentConfig developmentConfig;

    @Around("@annotation(com.cowboy.shooter.aspect.auth.role.HasRole)")
    public Object checkRole(ProceedingJoinPoint joinPoint) throws Throwable {
        if (developmentConfig.isDisableAuthorization()) {
            return joinPoint.proceed();
        }

        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();

        HasRole hasRole = method.getAnnotation(HasRole.class);
        Roles[] roles = hasRole.value();
        List<Roles> rolesList = Arrays.asList(roles);

        AccessToken accessToken = keycloakSecurityConfig.getAccessToken();
        boolean isAuthorized = hasRole.join().authorize(accessToken, rolesList);

        if (!isAuthorized) {
            throw new NotAuthorizedException("User [" + accessToken.getPreferredUsername() + "] is not authorized to use [" + signature.getName() + "]. He does not fulfill conditions of roles: " + hasRole.join() + " " + rolesList);
        }

        return joinPoint.proceed();
    }
}
