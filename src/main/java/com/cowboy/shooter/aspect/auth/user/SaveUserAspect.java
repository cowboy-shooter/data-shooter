package com.cowboy.shooter.aspect.auth.user;

import com.cowboy.shooter.config.DevelopmentConfig;
import com.cowboy.shooter.config.KeycloakSecurityConfig;
import com.cowboy.shooter.service.UserService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class SaveUserAspect {

    @Autowired
    private KeycloakSecurityConfig keycloakSecurityConfig;

    @Autowired
    private DevelopmentConfig developmentConfig;

    @Autowired
    private UserService userService;

    @Around("execution(* com.cowboy.shooter.graphql.*..*.*(..))")
    public Object saveUser(ProceedingJoinPoint joinPoint) throws Throwable {
        if (developmentConfig.isDisableAuthorization()) {
            return joinPoint.proceed();
        }

        AccessToken accessToken = keycloakSecurityConfig.getAccessToken();
        this.userService.createUser(accessToken.getSubject(), accessToken.getEmail(), accessToken.getGivenName(), accessToken.getFamilyName(), accessToken.getPreferredUsername());

        return joinPoint.proceed();
    }
}
