package com.cowboy.shooter.aspect.auth.role;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface HasRole {
    Roles[] value();
    HasRoleJoin join() default HasRoleJoin.ALL_OF;
}
