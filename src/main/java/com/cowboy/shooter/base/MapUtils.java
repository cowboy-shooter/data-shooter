package com.cowboy.shooter.base;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MapUtils {

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(Map.Entry.comparingByValue());

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

    public static void addToMap(Map<String, Integer> map, String key, Integer value) {
        if (map.containsKey(key)) {
            Integer foundValue = map.get(key);
            map.put(key, foundValue + value);
        } else {
            map.put(key, value);
        }
    }

    public static void addToMap(Map<String, Integer> map, String key) {
        if (map.containsKey(key)) {
            Integer foundValue = map.get(key);
            map.put(key, foundValue + 1);
        } else {
            map.put(key, 1);
        }
    }

}
