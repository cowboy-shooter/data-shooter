package com.cowboy.shooter.repository;

import com.cowboy.shooter.model.Team;
import com.cowboy.shooter.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TeamRepository extends JpaRepository<Team, String> {

    Optional<Team> findByMembersContains(User member);

    List<Team> findByInviteesContains(User invitee);

    List<Team> findByChallengedTeamsContains(Team team);

    Optional<Team> findByName(String name);

    Optional<Team> findByUrl(String url);

}
