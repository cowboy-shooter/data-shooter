package com.cowboy.shooter.repository;

import com.cowboy.shooter.model.TournamentTier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TournamentTierRepository extends JpaRepository<TournamentTier, String> {

}
