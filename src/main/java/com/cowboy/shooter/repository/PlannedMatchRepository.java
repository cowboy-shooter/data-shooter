package com.cowboy.shooter.repository;

import com.cowboy.shooter.model.PlannedMatch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlannedMatchRepository extends JpaRepository<PlannedMatch, String> {

}
