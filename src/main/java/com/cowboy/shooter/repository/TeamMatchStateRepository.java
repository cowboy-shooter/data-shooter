package com.cowboy.shooter.repository;

import com.cowboy.shooter.model.TeamMatchState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamMatchStateRepository extends JpaRepository<TeamMatchState, String> {

}
