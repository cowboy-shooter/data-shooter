package com.cowboy.shooter.repository;

import com.cowboy.shooter.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    Page<User> findAllByTeamIsNullAndIdNot(Pageable pageable, String id);

}
