package com.cowboy.shooter.repository;

import com.cowboy.shooter.model.Tournament;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TournamentRepository extends JpaRepository<Tournament, String> {

    Tournament findByFirstTier_IdOrSecondTier_IdOrThirdTier_IdOrFourthTier_Id(String firstTierId, String secondTierId, String thirdTierId, String fourthTierId);

}
