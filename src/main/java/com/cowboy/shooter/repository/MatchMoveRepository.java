package com.cowboy.shooter.repository;

import com.cowboy.shooter.model.MatchMove;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MatchMoveRepository extends JpaRepository<MatchMove, String> {

}
