package com.cowboy.shooter.repository;

import com.cowboy.shooter.model.Match;
import com.cowboy.shooter.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MatchRepository extends JpaRepository<Match, String> {

    List<Match> findByTeam1OrTeam2(Team team1, Team team2);

}
