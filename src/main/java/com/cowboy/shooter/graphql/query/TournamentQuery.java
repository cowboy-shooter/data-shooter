package com.cowboy.shooter.graphql.query;

import com.cowboy.shooter.aspect.auth.role.HasRole;
import com.cowboy.shooter.aspect.auth.role.Roles;
import com.cowboy.shooter.dto.PageableInput;
import com.cowboy.shooter.dto.PageableResponse;
import com.cowboy.shooter.dto.TournamentStatisticsDTO;
import com.cowboy.shooter.model.Tournament;
import com.cowboy.shooter.service.tournament.TournamentService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@GraphQLApi
public class TournamentQuery {

    @Autowired
    private TournamentService tournamentService;

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public PageableResponse<Tournament> getTournaments(@GraphQLArgument(name = "paging") PageableInput paging) {
        return tournamentService.getAllTournaments(PageableInput.convert(paging));
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public List<Tournament> getAllTournaments() {
        return tournamentService.getAllTournaments();
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public Optional<Tournament> getTournament(@GraphQLArgument(name = "id") String id) {
        return tournamentService.getTournament(id);
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public TournamentStatisticsDTO getTournamentStatistics(@GraphQLArgument(name = "id") String id) {
        return tournamentService.getStatistics(id);
    }

}
