package com.cowboy.shooter.graphql.query;

import com.cowboy.shooter.dto.PageableInput;
import com.cowboy.shooter.dto.PageableResponse;
import com.cowboy.shooter.model.Match;
import com.cowboy.shooter.service.MatchService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@GraphQLApi
public class MatchQuery {

    @Autowired
    private MatchService matchService;

    @GraphQLQuery
    public Optional<Match> getMatch(@GraphQLArgument(name = "matchId") String matchId) {
        return matchService.getMatch(matchId);
    }

    @GraphQLQuery
    public PageableResponse<Match> getMatches(@GraphQLArgument(name = "paging") PageableInput paging) {
        return matchService.getMatches(PageableInput.convert(paging));
    }

    @GraphQLQuery
    public List<Match> getParticipatedMatches(@GraphQLArgument(name = "teamId") String teamId) {
        return matchService.getParticipatedMatches(teamId);
    }

}
