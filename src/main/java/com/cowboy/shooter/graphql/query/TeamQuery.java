package com.cowboy.shooter.graphql.query;

import com.cowboy.shooter.aspect.auth.role.HasRole;
import com.cowboy.shooter.aspect.auth.role.Roles;
import com.cowboy.shooter.aspect.team.TeamStatusCheck;
import com.cowboy.shooter.aspect.team.TeamStatuses;
import com.cowboy.shooter.dto.PageableInput;
import com.cowboy.shooter.dto.PageableResponse;
import com.cowboy.shooter.dto.type.ProgrammingLanguagesType;
import com.cowboy.shooter.dto.wrapper.TeamChallengesManagementWrapper;
import com.cowboy.shooter.dto.wrapper.TeamUserManagementWrapper;
import com.cowboy.shooter.model.Team;
import com.cowboy.shooter.service.TeamService;
import com.cowboy.shooter.service.UserTeamService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@GraphQLApi
public class TeamQuery {

    @Autowired
    private TeamService teamService;

    @Autowired
    private UserTeamService userTeamService;

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public PageableResponse<Team> getTeams(@GraphQLArgument(name = "paging") PageableInput paging) {
        return this.teamService.getAllTeams(PageableInput.convert(paging));
    }

    @HasRole(value = Roles.ADMIN)
    @GraphQLQuery
    public List<Team> getAllTeams() {
        return this.teamService.getAllTeams();
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public Team getOwnTeam() {
        return userTeamService.getOwnTeam();
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public Optional<Team> getTeam(@GraphQLArgument(name = "id") String id) {
        return teamService.getTeam(id);
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public PageableResponse<TeamUserManagementWrapper> getOwnTeamManagement(@GraphQLArgument(name = "paging") PageableInput paging) {
        return userTeamService.getOwnTeamManagement(PageableInput.convert(paging));
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public PageableResponse<TeamChallengesManagementWrapper> getChallengesManagement(@GraphQLArgument(name = "paging") PageableInput paging) {
        return userTeamService.getOwnTeamChallengesManagement(PageableInput.convert(paging));
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public Map<String, String> getProgrammingLanguages() {
        return Arrays.stream(ProgrammingLanguagesType.values())
                .collect(Collectors.toMap(ProgrammingLanguagesType::toString, ProgrammingLanguagesType::getName));
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public Optional<Team> getTeamWithName(@GraphQLArgument(name = "name") String name) {
        return teamService.getTeamWithName(name);
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public Optional<Team> getTeamWithContext(@GraphQLArgument(name = "url") String url) {
        return teamService.getTeamWithUrl(url);
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public List<Team> getChallengers() {
        return userTeamService.getChallengers();
    }

}
