package com.cowboy.shooter.graphql.query;

import com.cowboy.shooter.aspect.auth.role.HasRole;
import com.cowboy.shooter.aspect.auth.role.Roles;
import com.cowboy.shooter.dto.PageableInput;
import com.cowboy.shooter.dto.PageableResponse;
import com.cowboy.shooter.model.User;
import com.cowboy.shooter.service.UserService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLQuery;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@GraphQLApi
public class UserQuery {

    @Autowired
    private UserService userService;

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public PageableResponse<User> getUsers(@GraphQLArgument(name = "paging") PageableInput paging) {
        return this.userService.getAllUsers(PageableInput.convert(paging));
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public PageableResponse<User> getUsersWithoutTeam(@GraphQLArgument(name = "paging") PageableInput paging) {
        return this.userService.getAllUsersWithoutTeam(PageableInput.convert(paging));
    }

    @HasRole(value = Roles.REGULAR)
    @GraphQLQuery
    public Optional<User> getUser(@GraphQLArgument(name = "id") String id) {
        return this.userService.getUser(id);
    }

}
