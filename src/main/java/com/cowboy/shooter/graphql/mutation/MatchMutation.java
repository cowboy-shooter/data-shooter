package com.cowboy.shooter.graphql.mutation;

import com.cowboy.shooter.aspect.auth.role.HasRole;
import com.cowboy.shooter.aspect.auth.role.Roles;
import com.cowboy.shooter.aspect.team.TeamStatusCheck;
import com.cowboy.shooter.aspect.team.TeamStatuses;
import com.cowboy.shooter.model.Match;
import com.cowboy.shooter.service.MatchExecutingService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@GraphQLApi
public class MatchMutation {

    @Autowired
    private MatchExecutingService matchExecutingService;

    @GraphQLMutation
    @HasRole(value = Roles.ADMIN)
    public Match startMatchSync(@GraphQLArgument(name = "team1Id") String team1Id,
                            @GraphQLArgument(name = "team2Id") String team2Id) {
        return matchExecutingService.startMatchAsync(team1Id, team2Id, false);
    }

    @GraphQLMutation
    @HasRole(value = Roles.ADMIN)
    public Match startMatchAsync(@GraphQLArgument(name = "team1Id") String team1Id,
                            @GraphQLArgument(name = "team2Id") String team2Id) {
        return matchExecutingService.startMatchSync(team1Id, team2Id, false);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public Match acceptChallenge(@GraphQLArgument(name = "teamId") String teamId) {
        return matchExecutingService.acceptChallenge(teamId);
    }

}
