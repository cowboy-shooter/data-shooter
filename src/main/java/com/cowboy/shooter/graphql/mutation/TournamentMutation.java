package com.cowboy.shooter.graphql.mutation;

import com.cowboy.shooter.aspect.auth.role.HasRole;
import com.cowboy.shooter.aspect.auth.role.Roles;
import com.cowboy.shooter.model.Tournament;
import com.cowboy.shooter.service.tournament.TournamentService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@GraphQLApi
public class TournamentMutation {

    @Autowired
    private TournamentService tournamentService;

    @GraphQLMutation
    @HasRole(value = Roles.ADMIN)
    public Tournament createTournament(@GraphQLArgument(name = "teamIds") List<String> teamIds, @GraphQLArgument(name = "name") String name) {
        return tournamentService.createTournament(teamIds, name);
    }

    @GraphQLMutation
    @HasRole(value = Roles.ADMIN)
    public void executeTier(@GraphQLArgument(name = "id") String id) {
        tournamentService.executeTier(id);
    }

    @GraphQLMutation
    @HasRole(value = Roles.ADMIN)
    public void fixTier(@GraphQLArgument(name = "id") String id, @GraphQLArgument(name = "teamIds") List<String> teamIds) {
        tournamentService.fixTier(id, teamIds);
    }

}
