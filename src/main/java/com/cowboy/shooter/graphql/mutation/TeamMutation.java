package com.cowboy.shooter.graphql.mutation;

import com.cowboy.shooter.aspect.auth.role.HasRole;
import com.cowboy.shooter.aspect.auth.role.Roles;
import com.cowboy.shooter.aspect.team.TeamStatusCheck;
import com.cowboy.shooter.aspect.team.TeamStatuses;
import com.cowboy.shooter.dto.type.ProgrammingLanguagesType;
import com.cowboy.shooter.model.Team;
import com.cowboy.shooter.service.UserTeamService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@GraphQLApi
public class TeamMutation {

    @Autowired
    private UserTeamService userTeamService;

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_NO_TEAM)
    public Team createTeam(@GraphQLArgument(name = "name") String name,
                           @GraphQLArgument(name = "language") ProgrammingLanguagesType language,
                           @GraphQLArgument(name = "description") String description,
                           @GraphQLArgument(name = "url") String url,
                           @GraphQLArgument(name = "flag") String flag) {
        return userTeamService.createTeam(name, language, description, url, flag);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.IS_LEADER_OF_TEAM)
    public Team updateTeam(@GraphQLArgument(name = "name") String name,
                           @GraphQLArgument(name = "language") ProgrammingLanguagesType language,
                           @GraphQLArgument(name = "description") String description,
                           @GraphQLArgument(name = "url") String url,
                           @GraphQLArgument(name = "flag") String flag) {
        return userTeamService.updateTeam(name, language, description, url, flag);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public void leaveTeam() {
        userTeamService.leaveTeam();
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public void inviteMember(@GraphQLArgument(name = "userId") String userId) {
        userTeamService.inviteMember(userId);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public void cancelInvite(@GraphQLArgument(name = "userId") String userId) {
        userTeamService.cancelInvite(userId);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_NO_TEAM)
    public void requestJoining(@GraphQLArgument(name = "teamId") String teamId) {
        userTeamService.requestJoining(teamId);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_NO_TEAM)
    public void cancelRequestJoining(@GraphQLArgument(name = "teamId") String teamId) {
        userTeamService.cancelRequestJoining(teamId);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public void acceptRequest(@GraphQLArgument(name = "userId") String userId) {
        userTeamService.acceptRequest(userId);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_NO_TEAM)
    public void acceptRequestJoining(@GraphQLArgument(name = "teamId") String teamId) {
        userTeamService.acceptRequestJoining(teamId);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.IS_LEADER_OF_TEAM)
    public void kickMemberOut(@GraphQLArgument(name = "userId") String userId) {
        userTeamService.removeMember(userId);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.IS_LEADER_OF_TEAM)
    public void makeMemberLeader(@GraphQLArgument(name = "userId") String userId) {
        userTeamService.makeMemberLeader(userId);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.IS_LEADER_OF_TEAM)
    public void changeFlag(@GraphQLArgument(name = "flag") String flag) {
        userTeamService.changeFlag(flag);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public void challengeTeam(@GraphQLArgument(name = "teamId") String teamId) {
        userTeamService.challengeTeam(teamId);
    }

    @GraphQLMutation
    @TeamStatusCheck(TeamStatuses.HAS_TEAM)
    public void cancelChallenge(@GraphQLArgument(name = "teamId") String teamId) {
        userTeamService.cancelChallenge(teamId);
    }

}
