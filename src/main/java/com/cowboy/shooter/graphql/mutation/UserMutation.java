package com.cowboy.shooter.graphql.mutation;

import com.cowboy.shooter.aspect.auth.role.HasRole;
import com.cowboy.shooter.aspect.auth.role.Roles;
import com.cowboy.shooter.model.User;
import com.cowboy.shooter.service.UserService;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.spqr.spring.annotations.GraphQLApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@GraphQLApi
public class UserMutation {

    @Autowired
    private UserService userService;

    @GraphQLMutation
    @HasRole(value = Roles.ADMIN)
    public User createUser(
                    @GraphQLArgument(name = "id") String id,
                    @GraphQLArgument(name = "email") String email,
                    @GraphQLArgument(name = "firstName") String firstName,
                    @GraphQLArgument(name = "lastName") String lastName,
                    @GraphQLArgument(name = "username") String username) {
        return this.userService.createUser(id, email, firstName, lastName, username);
    }

}
